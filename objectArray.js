/*We can also have an array of objects. We can group together
objects in an array.
That array can also use array methods for thee objects.However being
objext are more complex than strings or numbers. there is a 
difference when handling them. 

*/

let user = [
{
	name: "Mike Shell",
	username: "mikeBlueShell",
	email: "mikeyShell01@gmail.com",
	password: "iamikey1999",
	isActive: true,
	dateJoined: "August 8, 2011"
},
{
	name: "Jake Janella",
	username: "jjnella99",
	email: "jakejanella_99@gmail.com",
	password: "jackiejackie2",
	isActive: true,
	dateJoined: "January 14,2015"
}

];

console.log(user[0]);

/* how can we access inside the objectect property

*/
console.log(user[1].email);

// updating object inside array JSON

user[0].email = "mikeKingOfShell@gmail.com";
user[1].email = "janellajakeArchitect@gmail.com";
console.log(user);

// Can we also use array methods for array of objects?
//add another user ino array: push
user.push({
	name: "James Jameson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenace64",
	isActive: true,
	dateJoined: "Febraury 14,2021"

});
console.log(user);
//NEW SCHOOL METHOD
class User {
	constructor(name, username, email, password){
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.isActive = true;
		this.dateJoined = "September 28, 2021"
	}


}

let newUser1 = new User("Kate Middletown", "notTheDuchess", "seriouslyNotDuchess@gmail.com", "notRoyaltyAtAll")

console.log(newUser1);
user.push(newUser1);
console.log(user);


//find() is able to return to found item 
function login(username, password){
	//check if the useername does not exist or any uses that username
	let userFound = user.find((users) => {
		
		if(users.username === username && users.password === password){  
			console.log(`Thank you for loggin in ${username}`)
		
		}
		else if (users.username !== username && users.password !== password){
			return;
		}
		else {
			console.log(`Login Failed Invalid Credentials`)
		}

	})

}

login ("mikeBlueShell", "iamikey1999")
login ("mikeBlueShella", "iamikey1999")
login ("mikeBlueShell", "iamikey1999")